from flask import Flask
from flask import abort
application = Flask(__name__)

@application.route('/')
def hello_world():
    return 'Hello, World V4!!!!'

@application.route('/fr')
def bad_request():
    abort(400)

application.add_url_rule('/', 'index', (hello_world))

@application.route('/health')
def health():
    return { "app": "ok","db_connection": "ok","version": "1.0.0"}


if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    application.run(host='0.0.0.0')
