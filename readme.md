# Proof of concept Python HTTP API
This example app is used to demonstrate the canary deploiement on AWS Elastic Beanstalk

## Build app :
pip install -r requirements.txt

## Unit tests
pytest tests.py

## Run app:
export FLASK_APP=application.py flask run

## Run on Beanstalk
eb init -p 'Python 3.8' Arnaud-App --region eu-west-1; eb create Env1 --elb-type application > /dev/null 2>&1 || FAILED=true

